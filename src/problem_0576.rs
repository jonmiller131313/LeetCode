struct Solution;
// - - - -

use std::collections::HashMap;

type MemoMap = HashMap<MemoInfo, u128>;

#[derive(Eq, Hash, PartialEq)]
struct MemoInfo {
    x: i32,
    y: i32,
    left: i32,
}

impl Solution {
    pub fn find_paths(m: i32, n: i32, max_move: i32, start_row: i32, start_column: i32) -> i32 {
        let mut info = MemoMap::new();
        (find_paths_r(&mut info, m, n, max_move, start_row, start_column) % (10u128.pow(9) + 7)) as _
    }
}

fn find_paths_r(memo: &mut MemoMap, m: i32, n: i32, max_move: i32, start_row: i32, start_column: i32) -> u128 {
    let mut num_moves = 0;

    let info = MemoInfo {
        x: start_column,
        y: start_row,
        left: max_move,
    };
    if let Some(&left) = memo.get(&info) {
        return left;
    }

    // Base case
    if max_move == 0 {
        return 0;
    } else if max_move == 1 {
        if start_row == 0 {
            num_moves += 1;
        }
        if start_row == m - 1 {
            num_moves += 1;
        }
        if start_column == 0 {
            num_moves += 1;
        }
        if start_column == n - 1 {
            num_moves += 1;
        }
        return num_moves;
    }

    let mut check_and_recurse = |x: i32, y: i32| {
        if x >= 0 && x < n && y >= 0 && y < m {
            num_moves += find_paths_r(memo, m, n, max_move - 1, y, x);
        } else {
            num_moves += 1;
        }
    };

    // Recurse
    for delta in [-1, 1] {
        check_and_recurse(start_column + delta, start_row);
        check_and_recurse(start_column, start_row + delta);
    }

    memo.insert(info, num_moves);

    num_moves
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(6, Solution::find_paths(2, 2, 2, 0, 0));
    }

    #[test]
    fn test_2() {
        assert_eq!(12, Solution::find_paths(1, 3, 3, 0, 1));
    }

    #[test]
    fn test_3() {
        assert_eq!(0, Solution::find_paths(10, 10, 0, 5, 5));
    }

    #[test]
    fn test_4() {
        assert_eq!(914783380, Solution::find_paths(8, 50, 23, 5, 26));
    }
}
