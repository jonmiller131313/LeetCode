struct Solution;

use std::{cmp::max, collections::HashMap};

type Memo = HashMap<(usize, usize), usize>;

impl Solution {
    pub fn longest_common_subsequence(text1: String, text2: String) -> i32 {
        let mut memo = Memo::new();
        lcs(&mut memo, text1.as_bytes(), text2.as_bytes(), text1.len(), text2.len()) as _
    }
}

fn lcs(memo: &mut Memo, x: &[u8], y: &[u8], i: usize, j: usize) -> usize {
    if i == 0 || j == 0 {
        return 0;
    } else if let Some(&l) = memo.get(&(i, j)) {
        return l;
    }

    let x_i = &x[i - 1];
    let y_i = &y[j - 1];

    let l = if x_i == y_i {
        lcs(memo, x, y, i - 1, j - 1) + 1
    } else {
        max(lcs(memo, x, y, i, j - 1), lcs(memo, x, y, i - 1, j))
    };

    memo.insert((i, j), l);
    l
}

#[cfg(test)]
mod tests {
    use super::*;

    fn run(s1: impl Into<String>, s2: impl Into<String>) -> i32 {
        Solution::longest_common_subsequence(s1.into(), s2.into())
    }

    #[test]
    fn test_1() {
        assert_eq!(3, run("abcde", "ace"));
    }

    #[test]
    fn test_2() {
        assert_eq!(3, run("abc", "abc"));
    }

    #[test]
    fn test_3() {
        assert_eq!(0, run("abc", "def"));
    }
}
