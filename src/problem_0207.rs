struct Solution;

impl Solution {
    pub fn contains_duplicate(nums: Vec<i32>) -> bool {
        use std::collections::HashSet;

        let mut entries = HashSet::with_capacity(nums.len());
        nums.into_iter().any(|v| !entries.insert(v))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(true, Solution::contains_duplicate([1, 2, 3, 1].into()));
    }

    #[test]
    fn test_2() {
        assert_eq!(false, Solution::contains_duplicate([1, 2, 3, 4].into()));
    }

    #[test]
    fn test_3() {
        assert_eq!(
            true,
            Solution::contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2].into())
        );
    }
}
