struct Solution;

impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        let mut prefixes = Vec::with_capacity(nums.len());
        let mut suffixes = Vec::with_capacity(nums.len());

        for i in 0..nums.len() {
            let i = i as i32;
            let r = nums.len() as i32 - 1 - i;

            prefixes.push(prefixes.get((i - 1) as usize).unwrap_or(&1) * nums.get((i - 1) as usize).unwrap_or(&1));
            suffixes.push(suffixes.get((i - 1) as usize).unwrap_or(&1) * nums.get((r + 1) as usize).unwrap_or(&1));
        }

        prefixes
            .iter_mut()
            .zip(suffixes.into_iter().rev())
            .for_each(|(p, s)| *p *= s);
        return prefixes;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_one() {
        assert_eq!(Solution::product_except_self(vec![1, 2, 3, 4]), vec![24, 12, 8, 6]);
    }

    #[test]
    fn test_two() {
        assert_eq!(
            Solution::product_except_self(vec![-1, 1, 0, -3, 3]),
            vec![0, 0, 9, 0, 0]
        );
    }
}
