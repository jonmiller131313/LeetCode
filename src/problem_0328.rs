use std::fmt::{Debug, Formatter};

struct Solution;

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}
impl Debug for ListNode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}", self.val)?;

        let mut head = &self.next;

        while let Some(h) = head {
            write!(f, ", {}", h.val)?;
            head = &h.next;
        }

        return write!(f, "]");
    }
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    pub fn odd_even_list(mut head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        use std::mem::take;

        let (mut odd_front, mut even_front) = (None, None);
        let (mut odd_head, mut even_head) = (&mut odd_front, &mut even_front);
        let mut count = 0usize;

        while let Some(mut n) = head {
            head = take(&mut n.next);
            if count % 2 == 0 {
                odd_head = &mut odd_head.insert(n).next;
            } else {
                even_head = &mut even_head.insert(n).next;
            }
            count += 1;
        }

        if even_front.is_some() {
            *odd_head = even_front;
        }
        return odd_front;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_list(vals: impl IntoIterator<Item = i32>) -> Option<Box<ListNode>> {
        let mut vals = vals.into_iter();

        match vals.next() {
            Some(val) => Some(Box::new(ListNode {
                val,
                next: create_list(vals),
            })),
            None => None,
        }
    }

    #[test]
    fn test_create() {
        assert_eq!(
            create_list([1, 2, 3]),
            Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode {
                    val: 2,
                    next: Some(Box::new(ListNode::new(3)))
                }))
            }))
        )
    }

    #[test]
    fn test_one() {
        assert_eq!(
            Solution::odd_even_list(create_list([1, 2, 3, 4, 5])),
            create_list([1, 3, 5, 2, 4])
        );
    }

    #[test]
    fn test_two() {
        assert_eq!(
            Solution::odd_even_list(create_list([2, 1, 3, 5, 6, 4, 7])),
            create_list([2, 3, 6, 7, 1, 5, 4])
        );
    }
}
